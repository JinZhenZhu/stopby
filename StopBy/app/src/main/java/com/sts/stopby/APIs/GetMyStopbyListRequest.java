package com.sts.stopby.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.stopby.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class GetMyStopbyListRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_GET_MY_STOPBY_LIST;
    private Map<String,String> params;

    public GetMyStopbyListRequest(String userId, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_ID, userId);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
