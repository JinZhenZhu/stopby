package com.sts.stopby.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.stopby.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class LoginWithSocialAccountRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_LOGIN_WITH_SOCIAL_ACCOUNT;
    private Map<String,String> params;

    public LoginWithSocialAccountRequest(String userName, String email, String password, String token, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_NAME, userName);
        params.put(ReqConst.EMAIL, email);
        params.put(ReqConst.PASSWORD, password);
        params.put(ReqConst.TOKEN, token);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
