package com.sts.stopby.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.stopby.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class UploadCommentRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_COMMENT;
    private Map<String,String> params;

    public UploadCommentRequest(String userId, String stopbyId, String comment, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_ID, userId);
        params.put(ReqConst.STOP_BY_ID, stopbyId);
        params.put(ReqConst.COMMENT, comment);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
