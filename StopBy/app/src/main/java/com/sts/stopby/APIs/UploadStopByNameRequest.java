package com.sts.stopby.APIs;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.sts.stopby.Commons.ReqConst;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by STS on 2/9/2017.
 */

public class UploadStopByNameRequest extends StringRequest {

    private static  final String url =  ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_STOPBY_NAME;
    private Map<String,String> params;

    public UploadStopByNameRequest(String userId, String stobyName, String lat, String lng, Response.Listener<String> listener, Response.ErrorListener error) {

        super(Method.POST, url, listener,error);

        params = new HashMap<>();
        params.put(ReqConst.USER_ID, userId);
        params.put(ReqConst.STOP_BY_NAME, stobyName);
        params.put(ReqConst.LAT, lat);
        params.put(ReqConst.LNG, lng);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
