package com.sts.stopby.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.CustomView.SquareImageView;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.CommentModel;
import com.sts.stopby.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/27/2017.
 */

public class CommentListAdapter extends BaseAdapter {

    MainActivity activity;

    ArrayList<CommentModel> allData =  new ArrayList<>();

    public CommentListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<CommentModel> data){
        allData.clear();
        allData.addAll(data);
        this.notifyDataSetChanged();
    }

    public void addCommentModel(CommentModel oneData){
        allData.add(oneData);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public CommentModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView  = inflater.inflate(R.layout.item_user_list, parent, false);

            holder.setId(convertView);
            CommentModel data =  allData.get(position);
            holder.setData(data);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{

        RoundedImageView imvPhoto;
        ImageView imvLikeOrDislike, imvChat;
        TextView txvUserName, txvComment, txvMore;

        private void setId(View view){
            imvPhoto = (RoundedImageView)view.findViewById(R.id.imvPhoto);
            imvLikeOrDislike = (ImageView)view.findViewById(R.id.imvLikeOrDislike);
            txvUserName = (TextView)view.findViewById(R.id.txvUserName);
            imvChat = (ImageView)view.findViewById(R.id.imvChat);
            txvComment = (TextView)view.findViewById(R.id.txvComment);
            txvMore = (TextView)view.findViewById(R.id.txvMore);
            txvMore.setOnClickListener(this);
        }

        private void setData(CommentModel model){
            if (model.getPhotoUrl().length() > 0){
                Picasso.with(activity).load(model.getPhotoUrl()).into(imvPhoto);
            }

            if (model.getLikeOrDislike().equals(Constants.LIKE)){
                imvLikeOrDislike.setImageResource(R.drawable.like_red);
            }else if (model.getLikeOrDislike().equals(Constants.DISLIKE)){
                imvLikeOrDislike.setImageResource(R.drawable.dislike_grey);
            }else {
                imvLikeOrDislike.setImageResource(R.drawable.dont_know);
            }

            txvUserName.setText(model.getUserName());
            txvComment.setText(model.getComment());
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.txvMore:
                    txvComment.setSingleLine(false);
                    break;
            }
        }
    }
}
