package com.sts.stopby.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.R;

/**
 * Created by STS on 11/27/2017.
 */

public class FriendListAdapter extends BaseAdapter {

    MainActivity activity;

    public FriendListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView  = inflater.inflate(R.layout.item_friend, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class CustomHolder implements View.OnClickListener{

        ImageView imvChat;
        LinearLayout  lytUserInfo;
        private void setId(View view){
            imvChat =  (ImageView)view.findViewById(R.id.imvChat);
            imvChat.setOnClickListener(this);

            lytUserInfo = (LinearLayout)view.findViewById(R.id.lytUserInfo);
            lytUserInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imvChat:
                    activity.gotoChatFragment();
                    break;
                case R.id.lytUserInfo:
                    Commons.stopByOfUser = true;
                    activity.gotoStopBysForUserFragment();
                    break;
            }
        }
    }
}
