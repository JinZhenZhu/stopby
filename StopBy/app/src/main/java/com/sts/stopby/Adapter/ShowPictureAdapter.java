package com.sts.stopby.Adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.squareup.picasso.Picasso;
import com.sts.stopby.CustomView.SquareImageView;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.StopbyDetailModel;
import com.sts.stopby.Models.StopbyModel;
import com.sts.stopby.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/28/2017.
 */

public class ShowPictureAdapter extends PagerAdapter {

    MainActivity activity;
    ArrayList<StopbyDetailModel> allData = new ArrayList<>();

    public ShowPictureAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<StopbyDetailModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = LayoutInflater.from(activity);

        View view = inflater.inflate(R.layout.item_show_picture, container, false);

        SquareImageView imvPicture = (SquareImageView)view.findViewById(R.id.imvPicture);

        if (allData.get(position).getStopbyPicUrl().length() > 0){
            Picasso.with(activity).load(allData.get(position).getStopbyPicUrl()).into(imvPicture);
        }
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
