package com.sts.stopby.Adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

import com.makeramen.roundedimageview.RoundedImageView;
import com.sts.stopby.Fragment.HomeFragment;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.TempStopByModel;
import com.sts.stopby.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/23/2017.
 */

public class StopByAdapter extends BaseAdapter {

    MainActivity activity;
    HomeFragment mHomeFragment;

    ArrayList<TempStopByModel> allData =  new ArrayList<>();

    public StopByAdapter(MainActivity activity, HomeFragment fragment) {
        this.activity = activity;
        this.mHomeFragment = fragment;
    }

    public void refresh(ArrayList<TempStopByModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    public ArrayList<TempStopByModel> getAllData(){
        return allData;
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public TempStopByModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_stop_bys, parent, false);

            holder.setId(convertView);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        TempStopByModel model = allData.get(position);
        holder.setData(model, position);

        return convertView;
    }

    public class CustomHolder implements View.OnClickListener{

        TempStopByModel mStopByModel;
        int position = 0;

        public RoundedImageView imvPicture;
        EditText edtTitle, edtDescription;

        private void setId(View view){

            imvPicture = (RoundedImageView)view.findViewById(R.id.imvPicture);
            imvPicture.setOnClickListener(this);

            edtTitle = (EditText)view.findViewById(R.id.edtTitle);
            if (mHomeFragment.stopById == 0) edtTitle.setEnabled(false);
            edtTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {
                    allData.get(position).setTitle(edtTitle.getText().toString().trim());
                }
            });

            edtDescription = (EditText)view.findViewById(R.id.edtDescription);
            if (mHomeFragment.stopById == 0) edtDescription.setEnabled(false);
            edtDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {
                    allData.get(position).setDescription(edtDescription.getText().toString().trim());
                }
            });
        }

        private void setData(TempStopByModel model, int position){
            this.mStopByModel = model;
            this.position = position;

            if (model.getBmpPicture() != null){
                imvPicture.setImageBitmap(model.getBmpPicture());
            }

            edtTitle.setText(model.getTitle());
            edtDescription.setText(model.getDescription());
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imvPicture:
                    if (mHomeFragment.stopById == 0) return;
                    mHomeFragment.index = position;
                    mHomeFragment.takePhoto(position);
                    edtTitle.setEnabled(true); edtDescription.setEnabled(true);
                    break;
            }
        }
    }
}
