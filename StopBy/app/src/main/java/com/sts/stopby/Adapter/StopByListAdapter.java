package com.sts.stopby.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;
import com.sts.stopby.CustomView.SquareImageView;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.StopbyModel;
import com.sts.stopby.R;

import java.util.ArrayList;

/**
 * Created by STS on 11/27/2017.
 */

public class StopByListAdapter extends BaseAdapter {

    MainActivity activity;
    ArrayList<StopbyModel> allData =  new ArrayList<>();

    public StopByListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<StopbyModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public StopbyModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView  = inflater.inflate(R.layout.item_stop_by, parent, false);

            holder.setId(convertView);
            StopbyModel data = allData.get(position);
            holder.setData(data);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        return convertView;
    }

    private class CustomHolder{

        SquareImageView imvPicture;

        private void setId(View view){
            imvPicture = (SquareImageView)view.findViewById(R.id.imvPicture);
        }

        private void setData(StopbyModel model){
            if (model.getStopbyDetailModels().get(0).getStopbyPicUrl().length() > 0){
                Picasso.with(activity).load(model.getStopbyDetailModels().get(0).getStopbyPicUrl()).into(imvPicture);
            }
        }
    }
}
