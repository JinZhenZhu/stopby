package com.sts.stopby.Chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.R;

import java.util.ArrayList;

/**
 * Created by STS on 12/25/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private static final int CHAT_ME = 1;
    private static final int CHAT_FRIEND = 2;
    MainActivity activity;

    ArrayList<MessageModel> allDAta = new ArrayList<>();

    public ChatAdapter(MainActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<MessageModel> data){
        allDAta.clear();
        allDAta.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (allDAta.get(position).userId == Commons.user.getUserId())
            return CHAT_ME;
        return CHAT_FRIEND;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View customView;
        if (viewType == CHAT_ME){
            customView = LayoutInflater.from(activity).inflate(R.layout.item_chat_me, parent, false);
        }else {
            customView = LayoutInflater.from(activity).inflate(R.layout.item_chat_friend, parent, false);
        }
        return new ViewHolder(customView, viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        MessageModel chat = allDAta.get(position);

        if (getItemViewType(position) == CHAT_ME){

            holder.txvTimeStamp.setText(getTime(chat.getTimeStamp()));
            holder.txvMessage.setText(chat.getMessage());

        }else {

            if (chat.getPhotoUrl().length() > 0){

            }
        }
    }

    private String getTime(long timeStamp) {
        return "12:00";
    }

    @Override
    public int getItemCount() {
        return allDAta.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView txvTimeStamp, txvMessage, txvUserName;
        RoundedImageView imvPhoto;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            if (viewType == CHAT_ME){
                txvTimeStamp = (TextView)itemView.findViewById(R.id.txvTimeStamp);
                txvMessage = (TextView)itemView.findViewById(R.id.txvMessage);
            }else {
                imvPhoto = (RoundedImageView)itemView.findViewById(R.id.imvPhoto);
                txvUserName = (TextView)itemView.findViewById(R.id.txvUserName);
                txvTimeStamp = (TextView)itemView.findViewById(R.id.txvTimeStamp);
                txvMessage = (TextView)itemView.findViewById(R.id.txvMessage);
            }
        }
    }
}
