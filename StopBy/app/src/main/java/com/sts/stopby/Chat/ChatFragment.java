package com.sts.stopby.Chat;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.data.model.QiscusAccount;
import com.sts.stopby.Adapter.StopByListAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.R;

/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class ChatFragment extends BaseFragment {

    MainActivity activity;
    RecyclerView rvChat;

    public ChatFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_chat, container, false);

        rvChat = (RecyclerView)fragment.findViewById(R.id.rvChat);

        return fragment;
    }

    private void setUserInfo() {

        activity.showProgress();

        Qiscus.setUser(Commons.user.getEmail(), String.valueOf(Commons.user.getUserId()))
                .withUsername(Commons.user.getUserName())
                .withAvatarUrl(Commons.user.getPhotoUrl())
                .save(new Qiscus.SetUserListener() {
                    @Override
                    public void onSuccess(QiscusAccount qiscusAccount) {
                        activity.closeProgress();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        activity.showAlertDialog("Please check your internet");
                        activity.closeProgress();
                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvBack:
                break;
        }
    }
}
