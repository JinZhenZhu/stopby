package com.sts.stopby.Commons;

import com.sts.stopby.Models.StopbyModel;
import com.sts.stopby.Models.UserModel;

import java.util.ArrayList;

/**
 * Created by Li Ming on 11/21/2017.
 */

public class Commons {
    public static double lat = 0.0f;
    public static double lng = 0.0f;
    public static boolean stopByOfUser = false;
    public static boolean isAppRunning = false;
    public static UserModel user;
    public static String token = "";
    public static boolean refreshMyStopbyFragment = false;

    public static ArrayList<StopbyModel> myStopbyList = new ArrayList<>();
    public static StopbyModel selectedStopbyModel = new StopbyModel();
}
