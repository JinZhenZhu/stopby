package com.sts.stopby.Commons;

import android.graphics.Paint;

/**
 * Created by STS on 11/20/2017.
 */

public class Constants {
    public static final int SPLASH_TIME = 500;
    public static final int MY_PEQUEST_CODE = 104;
    public static final int LOCATION_UPDATE_TIME = 10000;//900000;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int PROFILE_IMAGE_SIZE = 256;
    public static final int VOLLEY_TIME_OUT = 120000;
    public static final String LIKE = "like";
    public static final String DISLIKE = "dislike";
}
