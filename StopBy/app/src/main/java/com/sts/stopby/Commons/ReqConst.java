package com.sts.stopby.Commons;

import java.net.PortUnreachableException;

/**
 * Created by STS on 12/3/2017.
 */

public class ReqConst {
    public static final String SERVER_URL = "http://stopby.online/index.php/api/";
    public static final String RES_CODE = "resultCode";
    public static final int CODE_GOOGLE_LOGIN = 10;
    public static final int CODE_SUCCESS = 100;
    public static final int CODE_EXIST_USER = 101;
    public static final int CODE_ERROR_LOGIN = 102;
    public static final int CODE_NO_MY_STOPBY = 103;
    public static final String SUCCESS = "success";
    public static final String MESSAGE = "message";

    /// Sign up
    public static final String  FUN_SIGNUP = "signup";
    public static final String USER_NAME = "userName";
    public static final String  EMAIL  ="email";
    public static final String PASSWORD = "password";
    public static final String USER_ID = "userId";
    public static final String PHOTO_URL = "photoUrl";

    // save token
    public static final String FUN_SAVE_TOKEN = "saveToken";
    public static final String TOKEN = "token";

    // login
    public static final String FUN_LOGIN = "login";
    public static final String USER_MODEL ="userModel";

    // login with social account
    public static final String FUN_LOGIN_WITH_SOCIAL_ACCOUNT = "loginWithSocialAccount";

    // upload stopby name
    public static final String FUN_UPLOAD_STOPBY_NAME = "uploadStopByName";
    public static final String STOP_BY_NAME = "stopbyName";
    public static final String STOP_BY_ID = "stopbyId";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String ADDRESS = "address";

    // upload stopby
    public static final String FUN_UPLOAD_STOPBY = "uploadStopBy";
    public static final String PARAM_FILE = "file";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";

    // upload photo
    public static final String FUN_UPLOAD_PHOTO = "uploadPhoto";

    // get my stopby list
    public static final String FUN_GET_MY_STOPBY_LIST = "getMyStopbyList";
    public static final String STOPBY_LIST = "stopbyList";
    public static final String STOPBY_DETAIL_ID = "stopbyDetailId";
    public static final String STOPBY_DETAIL_LIST = "stopbyDetailList";
    public static final String STOPBY_PIC_URL = "stopbyPicUrl";

    // get stopby list
    public static final String FUN_GET_STOPBY_LIST = "getStopbyList";

    // get comment list
    public static final String FUN_GET_COMMENT_LIST = "getCommentList";
    public static final String COMMENT_LIST = "commentList";
    public static final String COMMENT_ID = "commentId";
    public static final String LIKE_DISLIKE = "likeOrDislike";
    public static final String COMMENT = "comment";

    // upload comment
    public static final String FUN_UPLOAD_COMMENT = "uploadComment";
    public static final String COMMENT_MODEL = "commentModel";

    // like stopby
    public static final String FUN_LIKE_STOPBY = "likeStopby";

    // dislike stopby
    public static final String FUN_DISLIKE_STOPBY = "dislikeStopby";

    // get friend email
    public static final String FUN_GET_FRIEND_EMAIL = "getFriendEmail";
}
