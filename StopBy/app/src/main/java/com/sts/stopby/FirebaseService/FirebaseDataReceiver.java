package com.sts.stopby.FirebaseService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.sts.stopby.Commons.Commons;
import com.sts.stopby.R;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by STS on 5/25/2017.
 */

public class FirebaseDataReceiver extends WakefulBroadcastReceiver {

    public static boolean flagRunOne = false;

    private static String body = "", sound = "";
    Context _context ;
    private static int cnt = 0;

    public void onReceive(Context context, Intent intent) {

        this._context = context;

        if (!Commons.isAppRunning){
            ShortcutBadger.applyCount(context, cnt ++);
            showPushNotification();
        }
    }

    private void showPushNotification() {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final NotificationManager notificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(_context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(_context);
        builder
                .setContentTitle("Transform")
                .setContentText("Transform")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(contentIntent)
                .setSound(alarmSound);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }
}
