package com.sts.stopby.FirebaseService;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.service.QiscusFirebaseIdService;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Preference.PrefConst;
import com.sts.stopby.Preference.Preference;

/**
 * Created by STS on 5/25/2017.
 */

public class FirebaseIDService extends QiscusFirebaseIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Commons.token = refreshedToken;

        Preference.getInstance().put(this, PrefConst.FIREBASE_TOKEN, refreshedToken);
        Qiscus.setFcmToken(refreshedToken);
    }
}
