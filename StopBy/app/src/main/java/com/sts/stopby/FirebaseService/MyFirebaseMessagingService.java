package com.sts.stopby.FirebaseService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.qiscus.sdk.service.QiscusFirebaseService;

import java.util.Map;

/**
 * Created by STS on 5/25/2017.
 */

public class MyFirebaseMessagingService extends QiscusFirebaseService {
    private static final String TAG = "FCM Service";
    private Vibrator _vibrator;

    Context _context;

    String messageType = "", content = "", body = "", title = "";
    String orderId = "";
    static int cnt = 0;
    private static final String COMMON = "com.sts.chat";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        this._context = getApplicationContext();

        Map<String, String> data = remoteMessage.getData();

        title = remoteMessage.getNotification().getTitle();
        body = remoteMessage.getNotification().getBody();
    }
}
