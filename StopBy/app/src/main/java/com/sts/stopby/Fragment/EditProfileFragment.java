package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.makeramen.roundedimageview.RoundedImageView;
import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.data.model.QiscusAccount;
import com.squareup.picasso.Picasso;
import com.sts.stopby.Adapter.StopByListAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.TempStopByModel;
import com.sts.stopby.R;
import com.sts.stopby.StopByApplication;
import com.sts.stopby.Utils.BitmapUtils;
import com.sts.stopby.Utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class EditProfileFragment extends BaseFragment {

    MainActivity activity;
    RoundedImageView imvPhoto;
    TextView txvUserName, txvEmail;

    private Uri imageCaptureUri;
    String photoPath = "";

    public EditProfileFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        txvUserName = (TextView)fragment.findViewById(R.id.txvUserName);
        txvUserName.setText(Commons.user.getUserName());

        txvEmail = (TextView)fragment.findViewById(R.id.txvEmail);
        txvEmail.setText(Commons.user.getEmail());

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);
        imvPhoto.setOnClickListener(this);
        if (Commons.user.getPhotoUrl().length() > 0){
            Picasso.with(activity).load(Commons.user.getPhotoUrl()).into(imvPhoto);
        }

        return fragment;
    }

    private void takePhoto() {
        final String[] items = {"Take photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    takePhotoFromCamera();

                } else if (item == 1){
                    takePhotoFromGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(activity);

                        InputStream in = activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

//                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();

                        uploadPhoto();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(activity, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadPhoto() {
        try {
            activity.showProgress();
            File file = new File(photoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.USER_ID, String.valueOf(Commons.user.getUserId()));

            String url = ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_PHOTO;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.photo_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    activity.closeProgress();

                    try{
                        JSONObject response = new JSONObject(json);
                        String message =  response.getString(ReqConst.MESSAGE);
                        if (message.equals(ReqConst.SUCCESS)){
                            String photoUrl = response.getString(ReqConst.PHOTO_URL);
                            if (photoUrl.length() > 0){
                                Picasso.with(activity).load(photoUrl).into(imvPhoto);
                                Commons.user.setPhotoUrl(photoUrl);
                                updateProfileInChatingServer();
                            }
                        }else{
                            activity.showAlertDialog(getString(R.string.server_failed));
                        }
                    }catch (JSONException e){
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }

                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            StopByApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            activity.closeProgress();
            activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    private void updateProfileInChatingServer(){
        Qiscus.updateUser(Commons.user.getUserName(), Commons.user.getPhotoUrl(), new Qiscus.SetUserListener() {
            @Override
            public void onSuccess(QiscusAccount qiscusAccount) {
                //do anything after it successfully updated
            }

            @Override
            public void onError(Throwable throwable) {
                //do anything if error occurs
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvPhoto:
                takePhoto();
                break;
        }
    }
}
