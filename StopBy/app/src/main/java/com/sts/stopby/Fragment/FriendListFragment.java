package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sts.stopby.Adapter.FriendListAdapter;
import com.sts.stopby.Adapter.ShowPictureAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.R;
import com.viewpagerindicator.CirclePageIndicator;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class FriendListFragment extends BaseFragment {

    MainActivity activity;

    FriendListAdapter mFriendListAdapter;

    ListView lstFriendList;

    public FriendListFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_friend, container, false);

        mFriendListAdapter = new FriendListAdapter(activity);
        lstFriendList = (ListView)fragment.findViewById(R.id.lstFriendList);
        lstFriendList.setAdapter(mFriendListAdapter);

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.txvTitle.setText("Friend List");
        activity.imvBack.setVisibility(View.GONE);
    }
}
