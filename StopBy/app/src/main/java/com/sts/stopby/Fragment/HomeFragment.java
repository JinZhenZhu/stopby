package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;
import com.sts.stopby.APIs.UploadStopByNameRequest;
import com.sts.stopby.Adapter.StopByAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.TempStopByModel;
import com.sts.stopby.R;
import com.sts.stopby.StopByApplication;
import com.sts.stopby.Utils.BitmapUtils;
import com.sts.stopby.Utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class HomeFragment extends BaseFragment implements OnMapReadyCallback {

    MainActivity activity;
    StopByAdapter mStopByAdapter;

    MapView mapView;
    ListView lstMyStopBys;
    FloatingActionButton fbAddStopBy;
    Button btnStopBy;
    EditText edtPlaceName;

    GoogleMap googleMap;
    LatLng position = null;
    private Uri imageCaptureUri;
    public int index = 0;
    String photoPath = "", stopbyName = "", title = "", description = "";
    public int stopById = 0;

    ArrayList<TempStopByModel> mTempStopByModelList = new ArrayList<>();

    public HomeFragment(MainActivity activity) {
        this.activity = activity;
        position = new LatLng(Commons.lat, Commons.lng);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_home, container, false);

        edtPlaceName = (EditText)fragment.findViewById(R.id.edtPlaceName);

        btnStopBy = (Button)fragment.findViewById(R.id.btnStopBy);
        btnStopBy.setOnClickListener(this);

        mapView = (MapView) fragment.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.onResume();

        mStopByAdapter = new StopByAdapter(activity, this);
        lstMyStopBys = (ListView)fragment.findViewById(R.id.lstMyStopBys);
        lstMyStopBys.setAdapter(mStopByAdapter);

        TempStopByModel model = new TempStopByModel();
        mTempStopByModelList.add(model);
        mStopByAdapter.refresh(mTempStopByModelList);

        fbAddStopBy = (FloatingActionButton)fragment.findViewById(R.id.fbAddStopBy);
        fbAddStopBy.attachToListView(lstMyStopBys);
        fbAddStopBy.setOnClickListener(this);

        return fragment;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.addMarker(new MarkerOptions().position(position));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(latLng));
            }
        });
    }

    public void takePhoto(final int position) {

        final String[] items = {"Take photo", "Choose from Gallery", "Delete Item"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    takePhotoFromCamera();

                } else if (item == 1){
                    takePhotoFromGallery();
                }else {
                    deleteItem(position);
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
    }

    private void deleteItem(int position) {
        if (mTempStopByModelList.size() == 1) return;
        mTempStopByModelList.remove(position);
        mStopByAdapter.refresh(mTempStopByModelList);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(activity);

                        InputStream in = activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
//                        imvPhoto.setImageBitmap(bitmap);
                        photoPath = saveFile.getAbsolutePath();
                        mTempStopByModelList.get(index).setBmpPicture(bitmap);
                        mStopByAdapter.refresh(mTempStopByModelList);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    photoPath = BitmapUtils.getRealPathFromURI(activity, imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void uploadStopBy() {
        try {

            activity.showProgress();
            File file = new File(photoPath);

            Map<String, String> params = new HashMap<>();
            params.put(ReqConst.USER_ID, String.valueOf(Commons.user.getUserId()));
            params.put(ReqConst.STOP_BY_ID, String.valueOf(stopById));
            params.put(ReqConst.TITLE, title);
            params.put(ReqConst.DESCRIPTION, description);

            String url = ReqConst.SERVER_URL + ReqConst.FUN_UPLOAD_STOPBY;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.photo_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    activity.closeProgress();

                    mTempStopByModelList.clear();;
                    mTempStopByModelList.addAll(mStopByAdapter.getAllData());

                    TempStopByModel model = new TempStopByModel();
                    mTempStopByModelList.add(model);
                    mStopByAdapter.refresh(mTempStopByModelList);

                }
            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            StopByApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            activity.closeProgress();
            activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.txvTitle.setText("Home");
        activity.imvBack.setVisibility(View.GONE);
    }

    private boolean valid() {

        stopbyName = edtPlaceName.getText().toString().trim();
        if (stopbyName.length() == 0){
            activity.showBottomToast("Input place name");
            return false;
        }
        return true;
    }

    private boolean valid_1() {

        title = mTempStopByModelList.get(mTempStopByModelList.size() - 1).getTitle();
        description = mTempStopByModelList.get(mTempStopByModelList.size() - 1).getDescription();

        if (title.length() == 0){
            activity.showAlertDialog("Please input title");
            return false;
        }else if (description.length() == 0){
            activity.showAlertDialog("Please input description");
            return false;
        }

        return true;
    }

    private void uploadStopByName() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){
                        stopById = response.getInt(ReqConst.STOP_BY_ID);
                        activity.showBottomToast("Welcome new STOPBY\nPlease take picture of STOPBY");
                        mStopByAdapter.notifyDataSetChanged();
                    }else{
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        UploadStopByNameRequest req = new UploadStopByNameRequest(String.valueOf(Commons.user.getUserId()), stopbyName, String.valueOf(Commons.lat), String .valueOf(Commons.lng), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void showDialogToUploadNewStopBy(){
        new BottomDialog.Builder(activity)
                .setTitle("Awesome!")
                .setContent("Are you going to upload new STOPBY ?")
                .setPositiveText("Yes")
                .setNegativeText("No")
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(BottomDialog dialog) {
                        mTempStopByModelList.clear();
                        TempStopByModel model = new TempStopByModel();
                        mTempStopByModelList.add(model);
                        mStopByAdapter.refresh(mTempStopByModelList);

                        stopById = 0;
                        uploadStopByName();
                    }
                }).show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fbAddStopBy:
                if (stopById == 0) return;
                if (valid_1()){uploadStopBy();}
                break;
            case R.id.btnStopBy:
                if (valid()){
                    showDialogToUploadNewStopBy();}
                break;
        }
    }
}
