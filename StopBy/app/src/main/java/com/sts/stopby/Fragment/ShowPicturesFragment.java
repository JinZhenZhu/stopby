package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.stopby.APIs.DislikeStopbyRequest;
import com.sts.stopby.APIs.LikeStopbyRequest;
import com.sts.stopby.Adapter.ShowPictureAdapter;
import com.sts.stopby.Adapter.StopByListAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.CommentModel;
import com.sts.stopby.Models.StopbyModel;
import com.sts.stopby.R;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class ShowPicturesFragment extends BaseFragment {

    MainActivity activity;

    ShowPictureAdapter mShowPictureAdapter;

    ViewPager vpPictures;
    CirclePageIndicator vpIndicator;
    TextView txvStopbyTitle, txvDescription;
    ImageView imvLike, imvChat, imvDislike;

    StopbyModel stopbyModel;

    public ShowPicturesFragment(MainActivity activity) {
        this.activity = activity;
        stopbyModel  = Commons.selectedStopbyModel;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_show_pictures, container, false);

        imvLike = (ImageView)fragment.findViewById(R.id.imvLike);
        imvLike.setOnClickListener(this);

        imvChat = (ImageView)fragment.findViewById(R.id.imvChat);
        imvChat.setOnClickListener(this);

        imvDislike = (ImageView)fragment.findViewById(R.id.imvDislike);
        imvDislike.setOnClickListener(this);

        mShowPictureAdapter = new ShowPictureAdapter(activity);
        vpPictures = (ViewPager)fragment.findViewById(R.id.vpPictures);
        vpPictures.setAdapter(mShowPictureAdapter);
        mShowPictureAdapter.refresh(stopbyModel.getStopbyDetailModels());

        vpIndicator = (CirclePageIndicator)fragment.findViewById(R.id.vpIndicator);
        vpIndicator.setViewPager(vpPictures);

        txvStopbyTitle = (TextView)fragment.findViewById(R.id.txvStopbyTitle);
        txvStopbyTitle.setText(stopbyModel.getStopbyName());

        txvDescription = (TextView)fragment.findViewById(R.id.txvDescription);
        txvDescription.setText(stopbyModel.getStopbyDetailModels().get(0).getDescription());

        vpIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
           @Override
           public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

           @Override
           public void onPageSelected(int position) {
               txvDescription.setText(stopbyModel.getStopbyDetailModels().get(position).getDescription());
           }

           @Override
           public void onPageScrollStateChanged(int state) {}
       });
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.txvTitle.setText("Stop Bys");
        activity.imvBack.setVisibility(View.VISIBLE);
    }

    private void likeStopby() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){
                        activity.showBottomToast("Like !");
                    }else{
                        activity.showBottomToast(message);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        LikeStopbyRequest req = new LikeStopbyRequest(String.valueOf(Commons.user.getUserId()), String.valueOf(stopbyModel.getStopbyId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void dislikeStopby() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        activity.showBottomToast("Dislike !");
                    }else{
                        activity.showBottomToast(message);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        DislikeStopbyRequest req = new DislikeStopbyRequest(String.valueOf(Commons.user.getUserId()), String.valueOf(stopbyModel.getStopbyId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvLike:
                likeStopby();
                break;
            case R.id.imvDislike:
                dislikeStopby();
                break;
        }
    }
}
