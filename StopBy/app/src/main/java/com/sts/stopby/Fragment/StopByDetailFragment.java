package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.data.model.QiscusAccount;
import com.squareup.picasso.Picasso;
import com.sts.stopby.APIs.DislikeStopbyRequest;
import com.sts.stopby.APIs.GetCommentListRequest;
import com.sts.stopby.APIs.GetFriendEmailRequest;
import com.sts.stopby.APIs.LikeStopbyRequest;
import com.sts.stopby.APIs.UploadCommentRequest;
import com.sts.stopby.Adapter.CommentListAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.CommentModel;
import com.sts.stopby.Models.StopbyModel;
import com.sts.stopby.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class StopByDetailFragment extends BaseFragment implements OnMapReadyCallback {

    MainActivity activity;
    CommentListAdapter mCommentListAdapter;

    MapView mapView;
    ListView lstUserList;
    LinearLayout lytComment, lytChat, lytLike, lytDislike, lytContainer;
    RelativeLayout rytComment;
    ImageView imvClose;
    RoundedImageView imvPicture, imvPhoto;
    EditText edtComment;
    Button btnPutComment;

    GoogleMap googleMap;
    LatLng position = null;
    StopbyModel stopbyModel = new StopbyModel();
    String comment = "";

    public StopByDetailFragment(MainActivity activity) {
        this.activity = activity;
        stopbyModel = Commons.selectedStopbyModel;
        position = new LatLng(stopbyModel.getLat(), stopbyModel.getLng());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_stop_by_detail, container, false);

        lytContainer = (LinearLayout)fragment.findViewById(R.id.lytContainer);
        if (stopbyModel.getUserId() == Commons.user.getUserId()) lytContainer.setVisibility(View.GONE);

        lytDislike = (LinearLayout)fragment.findViewById(R.id.lytDislike);
        lytDislike.setOnClickListener(this);

        lytLike = (LinearLayout)fragment.findViewById(R.id.lytLike);
        lytLike.setOnClickListener(this);

        imvPhoto = (RoundedImageView)fragment.findViewById(R.id.imvPhoto);
        if (stopbyModel.getPhotoUrl().length() > 0){
            Picasso.with(activity).load(stopbyModel.getPhotoUrl()).into(imvPhoto);
        }

        mapView = (MapView) fragment.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.onResume();

        btnPutComment = (Button)fragment.findViewById(R.id.btnPutComment);
        btnPutComment.setOnClickListener(this);

        edtComment = (EditText)fragment.findViewById(R.id.edtComment);

        mCommentListAdapter = new CommentListAdapter(activity);
        lstUserList = (ListView)fragment.findViewById(R.id.lstUserList);
        lstUserList.setAdapter(mCommentListAdapter);

        lytComment = (LinearLayout)fragment.findViewById(R.id.lytComment);
        lytComment.setOnClickListener(this);

        rytComment = (RelativeLayout)fragment.findViewById(R.id.rytComment);

        imvClose = (ImageView)fragment.findViewById(R.id.imvClose);
        imvClose.setOnClickListener(this);

        imvPicture = (RoundedImageView)fragment.findViewById(R.id.imvPicture);
        imvPicture.setOnClickListener(this);

        if (stopbyModel.getStopbyDetailModels().get(0).getStopbyPicUrl().length() > 0){
            Picasso.with(activity).load(stopbyModel.getStopbyDetailModels().get(0).getStopbyPicUrl()).into(imvPicture);
        }

        lytChat = (LinearLayout)fragment.findViewById(R.id.lytChat);
        lytChat.setOnClickListener(this);

        getCommentList();

        return fragment;
    }

    private void getCommentList() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        ArrayList<CommentModel> mCommentModelList = new ArrayList<>();
                        JSONArray jsonCommentModelList =  response.getJSONArray(ReqConst.COMMENT_LIST);

                        for (int i = 0 ; i < jsonCommentModelList.length(); i++){

                            CommentModel mCommentModel = new CommentModel();
                            JSONObject jsonCommentModel = (JSONObject) jsonCommentModelList.getJSONObject(i);

                            mCommentModel.setUserId(jsonCommentModel.getInt(ReqConst.USER_ID));
                            mCommentModel.setCommentId(jsonCommentModel.getInt(ReqConst.COMMENT_ID));
                            mCommentModel.setUserName(jsonCommentModel.getString(ReqConst.USER_NAME));
                            mCommentModel.setPhotoUrl(jsonCommentModel.getString(ReqConst.PHOTO_URL));
                            mCommentModel.setLikeOrDislike(jsonCommentModel.getString(ReqConst.LIKE_DISLIKE));
                            mCommentModel.setComment(jsonCommentModel.getString(ReqConst.COMMENT));

                            mCommentModelList.add(mCommentModel);
                        }

                        mCommentListAdapter.refresh(mCommentModelList);

                    }else{
                        activity.showBottomToast(message);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetCommentListRequest req = new GetCommentListRequest(String.valueOf(Commons.user.getUserId()), String.valueOf(stopbyModel.getStopbyId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.addMarker(new MarkerOptions().position(position));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(latLng));
            }
        });
    }

    private void giveComment() {
        rytComment.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.txvTitle.setText("Stop Bys");
        activity.imvBack.setVisibility(View.VISIBLE);
    }

    private void uploadComment() {

        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        CommentModel mCommentModel = new CommentModel();
                        JSONObject jsonCommentModel = response.getJSONObject(ReqConst.COMMENT_MODEL);

                        mCommentModel.setCommentId(jsonCommentModel.getInt(ReqConst.COMMENT_ID));
                        mCommentModel.setUserId(jsonCommentModel.getInt(ReqConst.USER_ID));
                        mCommentModel.setUserName(jsonCommentModel.getString(ReqConst.USER_NAME));
                        mCommentModel.setPhotoUrl(jsonCommentModel.getString(ReqConst.PHOTO_URL));
                        mCommentModel.setLikeOrDislike(jsonCommentModel.getString(ReqConst.LIKE_DISLIKE));
                        mCommentModel.setComment(jsonCommentModel.getString(ReqConst.COMMENT));

                        mCommentListAdapter.addCommentModel(mCommentModel);

                        activity.showBottomToast(message);
                        rytComment.setVisibility(View.GONE);


                    }else{
                        activity.showBottomToast(message);
                        rytComment.setVisibility(View.GONE);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        UploadCommentRequest req = new UploadCommentRequest(String.valueOf(Commons.user.getUserId()), String.valueOf(stopbyModel.getStopbyId()), comment, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);

    }

    private boolean valid() {

        comment = edtComment.getText().toString().trim();
        if (comment.length() == 0 ){
            activity.showAlertDialog("Please type comment");
            return false;
        }
        return true;
    }

    private void likeStopby() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        CommentModel mCommentModel = new CommentModel();
                        JSONObject jsonCommentModel = response.getJSONObject(ReqConst.COMMENT_MODEL);

                        mCommentModel.setCommentId(jsonCommentModel.getInt(ReqConst.COMMENT_ID));
                        mCommentModel.setUserId(jsonCommentModel.getInt(ReqConst.USER_ID));
                        mCommentModel.setUserName(jsonCommentModel.getString(ReqConst.USER_NAME));
                        mCommentModel.setPhotoUrl(jsonCommentModel.getString(ReqConst.PHOTO_URL));
                        mCommentModel.setLikeOrDislike(jsonCommentModel.getString(ReqConst.LIKE_DISLIKE));
                        mCommentModel.setComment(jsonCommentModel.getString(ReqConst.COMMENT));

                        for (int i =  0; i < mCommentListAdapter.getCount(); i ++){
                            if (mCommentModel.getUserId() == mCommentListAdapter.getItem(i).getUserId()){
                                ImageView imvLikeOrDislike = lstUserList.getChildAt(i).findViewById(R.id.imvLikeOrDislike);
                                imvLikeOrDislike.setImageResource(R.drawable.like_red);
                            }
                        }
                        activity.showBottomToast(message);
                        rytComment.setVisibility(View.GONE);

                    }else{
                        activity.showBottomToast(message);
                        rytComment.setVisibility(View.GONE);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        LikeStopbyRequest req = new LikeStopbyRequest(String.valueOf(Commons.user.getUserId()), String.valueOf(stopbyModel.getStopbyId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void dislikeStopby() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message =  response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        CommentModel mCommentModel = new CommentModel();
                        JSONObject jsonCommentModel = response.getJSONObject(ReqConst.COMMENT_MODEL);

                        mCommentModel.setCommentId(jsonCommentModel.getInt(ReqConst.COMMENT_ID));
                        mCommentModel.setUserId(jsonCommentModel.getInt(ReqConst.USER_ID));
                        mCommentModel.setUserName(jsonCommentModel.getString(ReqConst.USER_NAME));
                        mCommentModel.setPhotoUrl(jsonCommentModel.getString(ReqConst.PHOTO_URL));
                        mCommentModel.setLikeOrDislike(jsonCommentModel.getString(ReqConst.LIKE_DISLIKE));
                        mCommentModel.setComment(jsonCommentModel.getString(ReqConst.COMMENT));

                        for (int i =  0; i < mCommentListAdapter.getCount(); i ++){
                            if (mCommentModel.getUserId() == mCommentListAdapter.getItem(i).getUserId()){
                                ImageView imvLikeOrDislike = lstUserList.getChildAt(i).findViewById(R.id.imvLikeOrDislike);
                                imvLikeOrDislike.setImageResource(R.drawable.dislike_grey);
                            }
                        }

                        activity.showBottomToast(message);
                        rytComment.setVisibility(View.GONE);

                    }else{
                        activity.showBottomToast(message);
                        rytComment.setVisibility(View.GONE);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        DislikeStopbyRequest req = new DislikeStopbyRequest(String.valueOf(Commons.user.getUserId()), String.valueOf(stopbyModel.getStopbyId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    private void getFriendEmail() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                try{
                    JSONObject response = new JSONObject(json);
                    String message = response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        String friendEmail =  response.getString(ReqConst.EMAIL);

                        Qiscus.buildChatWith(friendEmail) //here we use email as userID. But you can make it whatever you want.
                                .build(activity, new Qiscus.ChatActivityBuilderListener() {
                                    @Override
                                    public void onSuccess(Intent intent) {
                                        activity.closeProgress();
                                        startActivity(intent);
                                    }
                                    @Override
                                    public void onError(Throwable throwable) {
                                        activity.closeProgress();
                                        activity.showAlertDialog("Please check your network");
                                    }
                                });

                    }else{
                        activity.closeProgress();
                        activity.showAlertDialog(getString(R.string.server_failed));
                    }
                }catch (JSONException e){
                    activity.closeProgress();
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        GetFriendEmailRequest req = new GetFriendEmailRequest(String.valueOf(stopbyModel.getStopbyId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lytComment:
                giveComment();
                break;
            case R.id.imvClose:
                rytComment.setVisibility(View.GONE);
                break;
            case R.id.imvPicture:
                activity.showShowPicturesFragment();
                break;
            case R.id.lytChat:
                getFriendEmail();
                break;
            case R.id.btnPutComment:
                if (valid()){
                    uploadComment();
                }
                break;
            case R.id.lytLike:
                likeStopby();
                break;
            case R.id.lytDislike:
                dislikeStopby();
                break;
        }
    }
}
