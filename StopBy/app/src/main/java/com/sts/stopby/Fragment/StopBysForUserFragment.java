package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.sts.stopby.Adapter.StopByListAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.R;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class StopBysForUserFragment extends BaseFragment {

    MainActivity activity;
    StopByListAdapter mStopByListAdapter;

    GridView gvStopBys;
    LinearLayout lytUserInfo;

    public StopBysForUserFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_stop_bys_of_user, container, false);

        mStopByListAdapter =  new StopByListAdapter(activity);
        gvStopBys = (GridView)fragment.findViewById(R.id.gvStopBys);
        gvStopBys.setAdapter(mStopByListAdapter);
        gvStopBys.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                activity.showStopByDetailFragment();
            }
        });

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.txvTitle.setText("Stop Bys of Friend");
        activity.imvBack.setVisibility(View.VISIBLE);
    }
}
