package com.sts.stopby.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.sts.stopby.APIs.GetMyStopbyListRequest;
import com.sts.stopby.APIs.GetStopbyListRequest;
import com.sts.stopby.Adapter.StopByListAdapter;
import com.sts.stopby.Base.BaseFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Main.MainActivity;
import com.sts.stopby.Models.StopbyDetailModel;
import com.sts.stopby.Models.StopbyModel;
import com.sts.stopby.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by STS on 10/19/2017.
 */

@SuppressLint("ValidFragment")
public class StopBysFragment extends BaseFragment {

    MainActivity activity;
    StopByListAdapter mStopByListAdapter;

    GridView gvStopBys;
    ImageView imvFlamingo;

    public StopBysFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragment = inflater.inflate(R.layout.fragment_stop_bys, container, false);

        imvFlamingo = (ImageView)fragment.findViewById(R.id.imvFlamingo);

        mStopByListAdapter =  new StopByListAdapter(activity);
        gvStopBys = (GridView)fragment.findViewById(R.id.gvStopBys);
        gvStopBys.setAdapter(mStopByListAdapter);
        gvStopBys.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Commons.selectedStopbyModel = mStopByListAdapter.getItem(position);
                activity.showStopByDetailFragment();
            }
        });

        getAllData();

        return fragment;
    }

    private void getAllData() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                activity.closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message = response.getString(ReqConst.MESSAGE);
                    if (message .equals(ReqConst.SUCCESS)){

                        imvFlamingo.setVisibility(View.GONE);

                        ArrayList<StopbyModel> mStopbyModelList = new ArrayList<>();
                        JSONArray jsonStopbyModelList = response.getJSONArray(ReqConst.STOPBY_LIST);

                        for (int i = 0; i < jsonStopbyModelList.length() ; i++){

                            JSONObject jsonStopbyModel = (JSONObject) jsonStopbyModelList.get(i);

                            StopbyModel mStopbyModel = new StopbyModel();
                            mStopbyModel.setStopbyId(jsonStopbyModel.getInt(ReqConst.STOP_BY_ID));
                            mStopbyModel.setUserId(jsonStopbyModel.getInt(ReqConst.USER_ID));
                            mStopbyModel.setStopbyName(jsonStopbyModel.getString(ReqConst.STOP_BY_NAME));
                            mStopbyModel.setLat(jsonStopbyModel.getDouble(ReqConst.LAT));
                            mStopbyModel.setLng(jsonStopbyModel.getDouble(ReqConst.LNG));
                            mStopbyModel.setAddress(jsonStopbyModel.getString(ReqConst.ADDRESS));
                            mStopbyModel.setPhotoUrl(jsonStopbyModel.getString(ReqConst.PHOTO_URL));

                            ArrayList<StopbyDetailModel> mStopbyDetailModelList =  new ArrayList<>();
                            JSONArray jsonStopbyDetailModelList = jsonStopbyModel.getJSONArray(ReqConst.STOPBY_DETAIL_LIST);

                            for (int j = 0 ; j < jsonStopbyDetailModelList.length() ; j++){
                                StopbyDetailModel mStopbyDetailModel=  new StopbyDetailModel();
                                JSONObject jsonStopbyDetailModel = (JSONObject) jsonStopbyDetailModelList.get(j);

                                mStopbyDetailModel.setStopbyDetailId(jsonStopbyDetailModel.getInt(ReqConst.STOPBY_DETAIL_ID));
                                mStopbyDetailModel.setTitle(jsonStopbyDetailModel.getString(ReqConst.TITLE));
                                mStopbyDetailModel.setDescription(jsonStopbyDetailModel.getString(ReqConst.DESCRIPTION));
                                mStopbyDetailModel.setStopbyPicUrl(jsonStopbyDetailModel.getString(ReqConst.STOPBY_PIC_URL));

                                mStopbyDetailModelList.add(mStopbyDetailModel);
                            }

                            mStopbyModel.setStopbyDetailModels(mStopbyDetailModelList);

                            mStopbyModelList.add(mStopbyModel);
                        }

                        mStopByListAdapter.refresh(mStopbyModelList);

                    }else {
                        imvFlamingo.setVisibility(View.VISIBLE);
                        activity.showBottomToast(message);
                    }
                }catch (JSONException e){
                    activity.showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeProgress();
                activity.showAlertDialog(getString(R.string.server_failed));
            }
        };

        activity.showProgress();
        GetStopbyListRequest req = new GetStopbyListRequest(String.valueOf(Commons.user.getUserId()), res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(activity);
        request.add(req);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.txvTitle.setText("Stop Bys");
        activity.imvBack.setVisibility(View.GONE);
    }
}
