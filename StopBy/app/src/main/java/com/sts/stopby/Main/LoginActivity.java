package com.sts.stopby.Main;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sts.stopby.APIs.LoginRequest;
import com.sts.stopby.APIs.LoginWithSocialAccountRequest;
import com.sts.stopby.Base.BaseActivity;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Models.UserModel;
import com.sts.stopby.Preference.PrefConst;
import com.sts.stopby.Preference.Preference;
import com.sts.stopby.R;
import com.sts.stopby.Services.GpsService;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends BaseActivity implements  GoogleApiClient.OnConnectionFailedListener{

    EditText edtEmail, edtPassword;
    Button btnLogin, btnSignup, btnForgotPassword;
    ImageView imvGoogleLogin, imvFacebookLogin;

    String email = "", password = "", token = "", userName;
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS, android.Manifest.permission.CAMERA, android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION};

    GoogleApiClient mGoogleApiClient;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkAllPermission();
        checkLocationService();
        loadLayout();
        initSocialLoginRequest();
    }

    private void loadLayout() {

        imvFacebookLogin = (ImageView)findViewById(R.id.imvFacebookLogin);
        imvFacebookLogin.setOnClickListener(this);

        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtPassword = (EditText)findViewById(R.id.edtPassword);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        btnSignup = (Button)findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);

        btnForgotPassword = (Button)findViewById(R.id.btnForgotPassword);
        btnForgotPassword.setOnClickListener(this);

        imvGoogleLogin = (ImageView)findViewById(R.id.imvGoogleLogin);
        imvGoogleLogin.setOnClickListener(this);

        autoLogin();
    }

    private void autoLogin() {
        email = Preference.getInstance().getValue(this, PrefConst.EMAIL, "");
        password =  Preference.getInstance().getValue(this, PrefConst.PASSWORD, "");
        token =  FirebaseInstanceId.getInstance().getToken();

        if (email.length() > 0){
            edtEmail.setText(email);
            edtPassword.setText(password);
            loginWithEmail();
        }
    }

    private boolean valid() {
        email = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
        token =  FirebaseInstanceId.getInstance().getToken();
        if (token.length() == 0){
            showAlertDialog("Please check your network!");
            return false;
        }

        if (email.length() == 0){
            showAlertDialog("Please input email");
            return false;
        }else if (password.length() == 0){
            showAlertDialog("Please input password");
            return false;
        }
        return true;
    }

    private void loginWithEmail() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message = response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        UserModel mUserModel = new UserModel();
                        JSONObject jsonUserModel = response.getJSONObject(ReqConst.USER_MODEL);

                        mUserModel.setUserId(jsonUserModel.getInt(ReqConst.USER_ID));
                        mUserModel.setUserName(jsonUserModel.getString(ReqConst.USER_NAME));
                        mUserModel.setEmail(jsonUserModel.getString(ReqConst.EMAIL));
                        mUserModel.setPhotoUrl(jsonUserModel.getString(ReqConst.PHOTO_URL));

                        Commons.user = mUserModel;

                        gotoMainActivity();

                    }else {
                        showAlertDialog(message);
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginRequest req = new LoginRequest(email, password, token, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void gotoMainActivity() {

        Preference.getInstance().put(this, PrefConst.EMAIL, email);
        Preference.getInstance().put(this, PrefConst.PASSWORD, password);
        Preference.getInstance().put(this, PrefConst.FIREBASE_TOKEN, token);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void gotoSingupActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        finish();
    }

    private void showConfirmEmailDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alart_confirm_email);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText edtEmail = (EditText)dialog.findViewById(R.id.edtEmail);
        final EditText edtVerifyCode = (EditText)dialog.findViewById(R.id.edtVerifyCode);

        final Button btnConfirmEmail = (Button)dialog.findViewById(R.id.btnConfirmEmail);
        btnConfirmEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edtEmail.getText().toString().trim();
                if (email.length() ==  0){
                    showBottomToast("Please input email");
                }else {
                    edtVerifyCode.setVisibility(View.VISIBLE);
                    btnConfirmEmail.setText("Send Verify Code");
                    showResetPasswordDialog();
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void showResetPasswordDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alart_reset_password);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        dialog.show();
    }

    //// Check GPS service is running.
    private void checkLocationService() {
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }
    }

    /////////////////////////////////////

    //==================== Permission========================================
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {}
        runGpsService();
    }
//==================================================================

    private void runGpsService(){
        Intent serviceIntent = new Intent(this, GpsService.class);
        startService(serviceIntent);
    }

    private void initSocialLoginRequest() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.sts.stopby",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void loginWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, ReqConst.CODE_GOOGLE_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ReqConst.CODE_GOOGLE_LOGIN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            getUserInfo(acct);

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Google Connect Fail", "Connection Failed");
    }

    private void getUserInfo(GoogleSignInAccount acct) {
        String firstName = acct.getDisplayName().split(" ")[0];
        String lastName = acct.getDisplayName().split(" ")[1];

        userName = firstName +" " + lastName;
        email = acct.getEmail();
        password = acct.getId();
        token =  FirebaseInstanceId.getInstance().getToken();

        if (userName.length() == 0 || email.length() == 0 || password.length()== 0 || token.length() == 0) {
            showAlertDialog("Please check your network");
        }else {
            registerUserInfoWithSocialAccount();
        }

    }

    private void registerUserInfoWithSocialAccount() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message = response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        UserModel mUserModel = new UserModel();
                        JSONObject jsonUserModel = response.getJSONObject(ReqConst.USER_MODEL);

                        mUserModel.setUserId(jsonUserModel.getInt(ReqConst.USER_ID));
                        mUserModel.setUserName(jsonUserModel.getString(ReqConst.USER_NAME));
                        mUserModel.setEmail(jsonUserModel.getString(ReqConst.EMAIL));
                        mUserModel.setPhotoUrl(jsonUserModel.getString(ReqConst.PHOTO_URL));

                        Commons.user = mUserModel;

                        gotoMainActivity();

                    }else{
                        showAlertDialog(message);
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        LoginWithSocialAccountRequest req = new LoginWithSocialAccountRequest(userName, email, password, token, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }
    private void loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        try {
                                            // Application code
                                            email = object.getString("email");

                                            String fullName = object.getString("name");
                                            String firstName = fullName.split(" ")[0];  String lastName = fullName.split(" ")[1];
                                            userName = firstName + " " + lastName;
                                            password = object.getString("id");

                                            token =  FirebaseInstanceId.getInstance().getToken();

                                            if (email.length() == 0 || userName.length() == 0 || password.length() == 0 || token.length() == 0){
                                                showAlertDialog("Please check your network");
                                            }else {
                                                registerUserInfoWithSocialAccount();
                                            }

                                        }catch (Exception e){}
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                if (valid()){
                    loginWithEmail();
                }
                break;
            case R.id.btnSignup:
                gotoSingupActivity();
                break;
            case R.id.btnForgotPassword:
                showConfirmEmailDialog();
                break;
            case R.id.imvGoogleLogin:
                loginWithGoogle();
                break;
            case R.id.imvFacebookLogin:
                loginWithFacebook();
                break;
        }
    }
}
