package com.sts.stopby.Main;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.qiscus.sdk.Qiscus;
import com.squareup.picasso.Picasso;
import com.sts.stopby.Base.BaseActivity;
import com.sts.stopby.Chat.ChatFragment;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Fragment.EditProfileFragment;
import com.sts.stopby.Fragment.FriendListFragment;
import com.sts.stopby.Fragment.HomeFragment;
import com.sts.stopby.Fragment.MyStopBysFragment;
import com.sts.stopby.Fragment.ShowPicturesFragment;
import com.sts.stopby.Fragment.StopByDetailFragment;
import com.sts.stopby.Fragment.StopBysForUserFragment;
import com.sts.stopby.Fragment.StopBysFragment;
import com.sts.stopby.Models.CommentModel;
import com.sts.stopby.Models.StopbyDetailModel;
import com.sts.stopby.Preference.PrefConst;
import com.sts.stopby.Preference.Preference;
import com.sts.stopby.R;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    HomeFragment mHomeFragment;
    StopBysFragment mStopBysFragment;
    StopByDetailFragment mStopByDetailFragment;
    ShowPicturesFragment mShowPicturesFragment;
    FriendListFragment mFriendListFragment;
    StopBysForUserFragment mStopBysForUserFragment;
    MyStopBysFragment mMyStopBysFragment;
    ChatFragment mChatFragment;
    EditProfileFragment mEditProfileFragment;

    NavigationView navMenu;
    View menu;
    DrawerLayout drawContainer;
    ActionBarDrawerToggle drawerToggle ;
    ImageView imvMenu;
    FrameLayout fragContainer;
    TextView txvHome, txvStopBys, txvFriendList, txvMyStopBys, txvEditProfile, txvUserName;
    public ImageView imvBack;
    public TextView txvTitle;
    Button btnLogout;
    RoundedImageView imvPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        loadLayout();

        registerUserIntoChattingServer();
    }

    private void loadLayout() {
        drawContainer = (DrawerLayout)findViewById(R.id.drawContainer);
        navMenu = (NavigationView)findViewById(R.id.navMenu);
        drawContainer.addDrawerListener(drawerToggle);
        navMenu.setNavigationItemSelectedListener(this);
        menu = navMenu.getHeaderView(0);

        imvBack = (ImageView)findViewById(R.id.imvBack);
        imvBack.setOnClickListener(this);

        imvMenu = (ImageView)findViewById(R.id.imvMenu);
        imvMenu.setOnClickListener(this);

        txvHome = (TextView)menu.findViewById(R.id.txvHome);
        txvHome.setOnClickListener(this);

        txvFriendList = (TextView)menu.findViewById(R.id.txvFriendList);
        txvFriendList.setOnClickListener(this);

        txvStopBys = (TextView)menu.findViewById(R.id.txvStopBys);
        txvStopBys.setOnClickListener(this);

        txvTitle = (TextView)findViewById(R.id.txvTitle);

        txvMyStopBys = (TextView)menu.findViewById(R.id.txvMyStopBys);
        txvMyStopBys.setOnClickListener(this);

        txvEditProfile = (TextView)menu.findViewById(R.id.txvEditProfile);
        txvEditProfile.setOnClickListener(this);

        btnLogout = (Button)menu.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);

        imvPhoto = (RoundedImageView)menu.findViewById(R.id.imvPhoto);
        if (Commons.user.getPhotoUrl().length() > 0){
            Picasso.with(this).load(Commons.user.getPhotoUrl()).into(imvPhoto);
        }

        txvUserName = (TextView)menu.findViewById(R.id.txvUserName);
        txvUserName.setText(Commons.user.getUserName());

        showHomeFragment();
    }

    private void showHomeFragment() {

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mHomeFragment = new HomeFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mHomeFragment);
        fragmentTransaction.commit();
    }

    private void showStopBysFragment(){

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mStopBysFragment = new StopBysFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mStopBysFragment);
        fragmentTransaction.commit();
    }

    public void showStopByDetailFragment(){

        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mStopByDetailFragment = new StopByDetailFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mStopByDetailFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    public void showShowPicturesFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mShowPicturesFragment = new ShowPicturesFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mShowPicturesFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    private void gotoFriendListFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mFriendListFragment = new FriendListFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mFriendListFragment);
        fragmentTransaction.commit();
    }

    public void gotoStopBysForUserFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mStopBysForUserFragment = new StopBysForUserFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mStopBysForUserFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    private void gotoMyStopBysFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mMyStopBysFragment = new MyStopBysFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mMyStopBysFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    public void gotoChatFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mChatFragment = new ChatFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mChatFragment);
        fragmentTransaction.addToBackStack(null).commit();
    }

    private void gotoEditProfileFragment(){
        FragmentTransaction fragmentTransaction =  getSupportFragmentManager().beginTransaction();
        mEditProfileFragment = new EditProfileFragment(this);
        fragmentTransaction.replace(R.id.fragContainer, mEditProfileFragment);
        fragmentTransaction.commit();
    }

    private void logout() {
        Preference.getInstance().put(this, PrefConst.EMAIL, "");
        Preference.getInstance().put(this, PrefConst.PASSWORD, "");

        Qiscus.clearUser();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    private void registerUserIntoChattingServer() {
        Qiscus.setUser(Commons.user.getEmail(), String.valueOf(Commons.user.getUserId()))
                .withUsername(Commons.user.getUserName())
                .withAvatarUrl(Commons.user.getPhotoUrl())
                .save()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(qiscusAccount -> {
                    Log.d("aaaaa", "User is registered into chatting server");
                }, throwable -> {
                    Log.d("aaaaa", "User is not registered into chatting server");
                });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvMenu:
                drawContainer.openDrawer(Gravity.END);
                break;
            case R.id.txvHome:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showHomeFragment();
                    }
                }, 300);
                break;
            case R.id.txvStopBys:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showStopBysFragment();
                    }
                }, 300);
                break;
            case R.id.imvBack:
                getSupportFragmentManager().popBackStackImmediate();
                break;
            case R.id.txvFriendList:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gotoFriendListFragment();
                    }
                }, 300);
                break;
            case R.id.txvMyStopBys:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gotoMyStopBysFragment();
                    }
                }, 300);
                break;
            case R.id.txvEditProfile:
                drawContainer.closeDrawers();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gotoEditProfileFragment();
                    }
                }, 300);
                break;
            case R.id.btnLogout:
                logout();
                break;
        }
    }
}
