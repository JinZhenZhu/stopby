package com.sts.stopby.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sts.stopby.APIs.SignupRequest;
import com.sts.stopby.Base.BaseActivity;
import com.sts.stopby.Commons.Commons;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.Commons.ReqConst;
import com.sts.stopby.Models.UserModel;
import com.sts.stopby.Preference.PrefConst;
import com.sts.stopby.Preference.Preference;
import com.sts.stopby.R;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends BaseActivity {

    TextView txvLogin;
    EditText edtUserName, edtEmail, edtPassword, edtConfirmPassword;
    Button btnSignup;

    String userName = "", email = "", password = "", confirmPassword = "", token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        loadLayout();
    }

    private void loadLayout() {
        txvLogin = (TextView)findViewById(R.id.txvLogin);
        txvLogin.setOnClickListener(this);

        edtUserName = (EditText)findViewById(R.id.edtUserName);
        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText)findViewById(R.id.edtConfirmPassword);

        btnSignup = (Button)findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);
    }

    private void gotoLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean valid() {
        userName = edtUserName.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
        confirmPassword =  edtConfirmPassword.getText().toString().trim();

        token =  FirebaseInstanceId.getInstance().getToken();
        if (token.length() == 0){
            showAlertDialog("Please check your network!");
            return false;
        }

        if (userName.length() == 0){
            showAlertDialog("Please input user name");
            return false;
        }else if (email.length() == 0){
            showAlertDialog("Please input email");
            return false;
        }else if (password.length() == 0){
            showAlertDialog("Please input password");
            return false;
        }else if (!password.equals(confirmPassword)){
            showAlertDialog("Please confirm password");
            return false;
        }
        return true;
    }

    private void signup() {
        final Response.Listener<String> res = new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                closeProgress();
                try{
                    JSONObject response = new JSONObject(json);
                    String message = response.getString(ReqConst.MESSAGE);
                    if (message.equals(ReqConst.SUCCESS)){

                        UserModel mUserModel = new UserModel();
                        mUserModel.setUserId(response.getInt(ReqConst.USER_ID));
                        mUserModel.setUserName(userName);
                        mUserModel.setEmail(email);

                        Commons.user = mUserModel;

                        gotoMainActivity();

                    }else{
                        showAlertDialog(message);
                    }
                }catch (JSONException e){
                    showAlertDialog(getString(R.string.server_failed));
                }
            }
        };
        final Response.ErrorListener error = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.server_failed));
            }
        };

        showProgress();
        SignupRequest req = new SignupRequest(userName, email, password, token, res, error);
        req.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue request = Volley.newRequestQueue(this);
        request.add(req);
    }

    private void gotoMainActivity() {
        Preference.getInstance().put(this, PrefConst.EMAIL, email);
        Preference.getInstance().put(this, PrefConst.PASSWORD, password);
        Preference.getInstance().put(this, PrefConst.FIREBASE_TOKEN, token);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txvLogin:
                gotoLoginActivity();
                break;
            case R.id.btnSignup:
                if (valid()) signup();
                break;
        }
    }
}
