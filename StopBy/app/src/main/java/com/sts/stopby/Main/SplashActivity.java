package com.sts.stopby.Main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sts.stopby.Base.BaseActivity;
import com.sts.stopby.Commons.Constants;
import com.sts.stopby.R;
import com.sts.stopby.Services.GpsService;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        gotoLoginActivity();
    }


    private void gotoLoginActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                closeProgress();
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
