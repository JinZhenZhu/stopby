package com.sts.stopby.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/24/2017.
 */

public class CommentModel implements Serializable {

    int commentId = 0, userId = 0;
    String userName = "", comment = "", photoUrl = "", likeOrDislike = "";

    public String getLikeOrDislike() {
        return likeOrDislike;
    }

    public void setLikeOrDislike(String likeOrDislike) {
        this.likeOrDislike = likeOrDislike;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
