package com.sts.stopby.Models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by STS on 12/20/2017.
 */

public class StopbyModel implements Serializable {

    int userId = 0, stopbyId = 0;
    String stopbyName = "", address = "", photoUrl = "";
    double lat = 0.0d, lng = 0.0d;

    ArrayList<StopbyDetailModel> stopbyDetailModels = new ArrayList<>();

    public ArrayList<StopbyDetailModel> getStopbyDetailModels() {
        return stopbyDetailModels;
    }

    public void setStopbyDetailModels(ArrayList<StopbyDetailModel> stopbyDetailModels) {
        this.stopbyDetailModels = stopbyDetailModels;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStopbyId() {
        return stopbyId;
    }

    public void setStopbyId(int stopbyId) {
        this.stopbyId = stopbyId;
    }

    public String getStopbyName() {
        return stopbyName;
    }

    public void setStopbyName(String stopbyName) {
        this.stopbyName = stopbyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
