package com.sts.stopby.Models;

import java.io.Serializable;

/**
 * Created by STS on 12/20/2017.
 */

public class StopbyDetailModel implements Serializable {
    int stopbyId = 0, stopbyDetailId = 0;
    String title = "", description = "", stopbyPicUrl = "";

    public int getStopbyId() {
        return stopbyId;
    }

    public void setStopbyId(int stopbyId) {
        this.stopbyId = stopbyId;
    }

    public int getStopbyDetailId() {
        return stopbyDetailId;
    }

    public void setStopbyDetailId(int stopbyDetailId) {
        this.stopbyDetailId = stopbyDetailId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStopbyPicUrl() {
        return stopbyPicUrl;
    }

    public void setStopbyPicUrl(String stopbyPicUrl) {
        this.stopbyPicUrl = stopbyPicUrl;
    }
}
