package com.sts.stopby.Models;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by STS on 11/23/2017.
 */

public class TempStopByModel implements Serializable {
    Bitmap bmpPicture;
    String title = "", description = "";

    public Bitmap getBmpPicture() {
        return bmpPicture;
    }

    public void setBmpPicture(Bitmap bmpPicture) {
        this.bmpPicture = bmpPicture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
