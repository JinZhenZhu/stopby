package com.sts.stopby.Preference;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String FIREBASE_TOKEN = "token";
}
